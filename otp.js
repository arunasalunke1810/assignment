window.onload = () => {

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    document.getElementById("otp-form-name").innerHTML = urlParams.get('first_name');
    document.getElementById("otp-form-phone").innerHTML = urlParams.get('mobile');

    var random_otp = urlParams.get('otp');
    var attempts = 1;
    
    var otp_form = document.getElementById("otp_form");
    var otp_input = document.getElementById("otp");

    otp_input.addEventListener("keydown", (e) => {
        if (((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode == 8 || e.keyCode == 9) {
            console.log("Acceptable key pressed");
        } else {
            e.preventDefault();
            return false;
        }
    });

    otp_input.addEventListener("input", (e) => {
        var otp = e.target.value;
        e.target.value = otp.substring(0, 4);
    });

    otp_form.addEventListener('submit', (e) => {
        e.preventDefault();
        var error = null;
        if (otp_input.value.trim() == random_otp) {
            document.getElementById("success-otp").innerHTML = 'Validated successfully!';
            error = null;
            window.location.href = 'http://pixel6.co/';
        } else if (otp_input.value.trim() && attempts == 3) {
            window.location.href = 'http://pixel6.co/404';
        } else if (otp_input.value.trim()) {
            otp_input.value = '';
            attempts++;
            error = 'Invalid OTP please try again!';
        } else {
            error = 'Enter 4 digit OTP';
        }

        document.getElementById("error-otp").innerHTML = error;

    });
}