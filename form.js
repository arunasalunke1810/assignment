window.onload = () => {
    var is_valid_phone = false;
    var is_valid_name = false;
    var is_valid_email = false;
    var first_name = null;
    var mobile_number = null;

    var fullname = document.getElementById("fullname");

    fullname.addEventListener("keydown", (e) => {
        if (!(e.keyCode >= 65 && e.keyCode <= 90) && (e.keyCode != 8) && (e.keyCode != 32) && (e.keyCode != 9)) {
            e.preventDefault();
            return false;
        }
    });

    fullname.addEventListener("input", (e) => {
        var name = e.target.value;

        if (name) {
            const name_array = name.trim().split(" ");
            if (name_array.length < 2) {
                is_valid_name = false;
                first_name = null;
            } else {
                for (let i = 0; i < name_array.length;i++){
                    var word = name_array[i];
                    if (word) {
                        if (word.length >= 4) {
                            is_valid_name = true;
                            if (i == 0) {
                                first_name = word;
                            }
                        } else {
                            first_name = null;
                            is_valid_name = false;
                            break;
                        }
                    }
                }
            }
        } else {
            is_valid_name = false;
            first_name = null;
        }
        if (is_valid_name) {
            document.getElementById("error-fullname").innerHTML = null;
        } else {
            document.getElementById("error-fullname").innerHTML = 'Enter valid name';
        }
    });

    var phonenumber = document.getElementById("phone-number");
    phonenumber.addEventListener("keydown", (e) => {
        if (((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57)) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode == 8 || e.keyCode == 9) {
            console.log("Acceptable key pressed");
        } else {
            e.preventDefault();
            return false;
        }
    });

    phonenumber.addEventListener("input", (e) => {

        var provider = document.getElementById("provider");
        var stateEle = document.getElementById("state");

        provider.innerHTML = '';
        stateEle.innerHTML = '';

        var phonenumber_value = e.target.value;
        var phonenumber_clean = phonenumber_value.replace(/\D/g, '');

        phonenumber_clean = phonenumber_clean.substring(0, 10);

        var match1 = phonenumber_clean.substring(0, 3);
        var match2 = phonenumber_clean.substring(3, 6);
        var match3 = phonenumber_clean.substring(6, 10);

        var formatted_mobile = '';
        if (match1 && match2) {
            formatted_mobile = '(' + match1 + ')';
        } else {
            formatted_mobile = match1;
        }
        if (match2) {
            formatted_mobile = formatted_mobile + '-' + match2;
        }
        if (match3) {
            formatted_mobile = formatted_mobile + '-' + match3;
        }
        e.target.value = formatted_mobile;

        var operator = getOperator(match1);
        var state = getState(match2);

        if (operator != 'Invalid number' && state != 'Invalid number') {
            provider.innerHTML = operator;
            stateEle.innerHTML = ', ' + state;
            is_valid_phone = true;
            mobile_number = phonenumber_clean;
        }
        else {
            provider.innerHTML = 'Invalid number';
            is_valid_phone = false;
            mobile_number = null;
        }
    });

    var email = document.getElementById("email");
    email.addEventListener("input", (e) => {
        var email_input = e.target.value;
        var email_input_validate = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z\-])+\.)+([a-zA-Z]{2,4})+$/;
        if (!email_input_validate.test(email_input)) {
            is_valid_email = false;
        } else {
            is_valid_email = true;
        }

        if (is_valid_email) {
            document.getElementById("error-email").innerHTML = null;
        } else {
            document.getElementById("error-email").innerHTML = 'Enter valid email address';
        }

    });

    var signup_form = document.getElementById("signup");

    signup_form.addEventListener('submit', (e) => {
        e.preventDefault();

        if (!is_valid_name || !is_valid_email || !is_valid_phone) {
            return false;
        }

        random_otp = Math.round(1000 + Math.random() * 9000);

        var input_first_name = document.getElementById('first_name');
        var input_mobile_number = document.getElementById('mobile_number');
        var input_otp = document.getElementById('otp');

        input_first_name.value = first_name;
        input_mobile_number.value = mobile_number;
        input_otp.value = random_otp;

        signup_form.submit();

    });

     const getOperator = (code) => {
        if (code >= 621 && code <= 799) {
            return "Reliance Jio";
        }
        else if (code >= 801 && code <= 920) {
            return "Idea";
        }
        else if (code >= 921 && code <= 999) {
            return "Vodafone";
        } else {
            return "Invalid number";
        }
    }

    const getState = (code) => {
        code = parseInt(code);
        var states = {
            1: 'Jammu and Kashmir',
            2: 'Himachal Pradesh',
            3: 'Punjab',
            4: 'Chandigarh',
            5: 'Uttarakhand',
            6: 'Haryana',
            7: 'Delhi',
            8: 'Rajasthan',
            9: 'Uttar Pradesh',
            10: 'Bihar',
            11: 'Sikkim',
            12: 'Arunachal Pradesh',
            13: 'Nagaland',
            14: 'Manipur',
            15: 'Mizoram',
            16: 'Tripura',
            17: 'Meghalaya',
            18: 'Assam',
            19: 'West Bengal',
            20: 'Jharkhand',
            21: 'Odisha',
            22: 'Chattisgarh',
            23: 'Madhya Pradesh',
            24: 'Gujarat',
            26: 'Dadra & Nagar Haveli and Daman & Diu',
            27: 'Maharashtra',
            29: 'Karnataka',
            30: 'Goa',
            31: 'Lakshadweep Islands',
            32: 'Kerala',
            33: 'Tamil Nadu',
            34: 'Pondicherry',
            35: 'Andaman and Nicobar Islands',
            36: 'Telangana',
            37: 'Andhra Pradesh',
            38: 'Ladakh',
            97: 'Other Territory'
        };

        return states[code] ? states[code] : "Invalid number";
    }
}